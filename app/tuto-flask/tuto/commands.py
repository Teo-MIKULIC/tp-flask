from .models import Author, Book
import yaml
import click
from .app import app, db


@app.cli.command()
@click.argument('filename')
def loaddb(filename):
    '''Creates the tables and populates them with data. '''

    # création de toutes les tables
    db.create_all()

    # chargement de notre jeu de données
    books = yaml.safe_load(open(filename))

    # première passe : création de tous les auteurs
    authors = {}
    for b in books:
        a = b["author"]
        if a not in authors:
            o = Author(name=a)
            db.session.add(o)
            authors[a] = o
    db.session.commit()

    # deuxi ème passe : création de tous les livres
    for b in books:
        a = authors[b["author"]]
        o = Book(price=b["price"],
                 title=b["title"],
                 url=b["url"],
                 img=b["img"],
                 author_id=a.id)
        db.session.add(o)
    db.session.commit()


@app.cli. command()
def syncdb():
    '''Creates all missing tables.'''
    db.create_all()


@app.cli. command()
@click.argument('username')
@click.argument('password')
def newuser(username, password):
    '''Adds a new user.'''
    from .models import User
    from hashlib import sha256
    m = sha256()
    m.update(password.encode())
    u = User(username=username, password=m.hexdigest())
    db.session.add(u)
    db.session.commit()


@app.cli. command()
@click.argument('idA')
@click.argument('nameA')
def newauthor(idA, nameA):
    '''Adds a new author.'''
    from .models import Author
    id = int(idA)
    name = str(nameA)
    a = Author(id=id, name=name)
    db.session.add(a)
    db.session.commit()


@app.cli. command()
def showusers():
    '''Shows all users.'''
    from .models import User
    for u in User.query.all():
        print(u.username)


@app.cli. command()
def showuserspass():
    '''Shows all users.'''
    from .models import User
    for u in User.query.all():
        print(u.username, u.password)


@app.cli. command()
def showauthors():
    '''Shows all authors.'''
    from .models import Author
    for a in Author.query.all():
        print(a.id, a.name)


@app.cli. command()
@click.argument('username')
@click.argument('password')
def passwd(username, password):
    '''Shows all users.'''
    from .models import User
    from hashlib import sha256
    m = sha256()
    m.update(password.encode())
    u = User.query.get(username)
    u.password = m.hexdigest()
    db.session.commit()


@app.cli. command()
@click.argument('username')
def deleteuser(username):
    '''Delete user'''
    from .models import User
    if User.query.get(username):
        u = User.query.get(username)
        db.session.delete(u)
        db.session.commit()
    else:
        print("User not found")


@app.cli. command()
@click.argument('id')
def showbook(id):
    from .models import Book
    b = Book.query.get(id)
    print(b.id, b.title, b.img)
    
@app.cli. command()
def showbooks():
    from .models import Book
    for b in Book.query.all():
        print(b.id, b.title, b.img)
        
@app.cli.command()
def showfav():
    from .models import Favoris
    for f in Favoris.query.all():
        print(f.id, f.username)