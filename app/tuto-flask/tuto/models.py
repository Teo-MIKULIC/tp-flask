from .app import db
from flask_login import UserMixin
from .app import login_manager


@login_manager.user_loader
def load_user(username):
    return User.query.get(username)


class Author (db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))

    def __repr__(self):
        return self.name


class User(db.Model, UserMixin):
    __tablename__ = "USER"
    username = db.Column(db.String(50), primary_key=True)
    password = db.Column(db.String(64))
    favoris = db.relationship("Favoris", backref="FAVORIS")

    def get_id(self):
        return self.username


class Book(db.Model):
    __tablename__ = "BOOK"
    id = db.Column(db.Integer, primary_key=True)
    price = db.Column(db.Float)
    title = db.Column(db.String(100))
    url = db.Column(db.String(100))
    img = db.Column(db.String(100))
    author_id = db.Column(db.Integer, db.ForeignKey("author.id"))
    author = db.relationship(
        "Author", backref=db.backref("books", lazy="dynamic"))

    def __repr__(self):
        return "<Book (%d) %s>" % (self.id, self.title)
    
class Favoris(db.Model):
    __tablename__ = "FAVORIS"
    id = db.Column("id",db.Integer,db.ForeignKey("BOOK.id"),primary_key=True)
    username = db.Column("username",db.String(50),db.ForeignKey("USER.username"),primary_key=True)
    
    def get_id_favoris_user(self, id : int, username : str):
        return self.id


def get_sample():
    return Book.query.all()


def get_author(id):
    return Author.query.get_or_404(id)


def get_id(self):
    return self.username


def get_book(id):
    return Book.query.get_or_404(id)
