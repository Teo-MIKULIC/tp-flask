import os.path
from flask_sqlalchemy import SQLAlchemy
from flask import Flask
from flask_bootstrap import Bootstrap5
from flask_login import LoginManager


def mkpath(p):
    return os.path. normpath(os.path.join(os.path. dirname(__file__), p))


app = Flask(__name__)
bootstrap = Bootstrap5(app)

app.config['BOOTSTRAP_SERVE_LOCAL'] = True
app.config['SECRET_KEY'] = "db9c33b5-29f9-40cf-9896-49f3c2ff03eb"
app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///' + mkpath('../myapp.db'))
login_manager = LoginManager(app)
login_manager.login_view = "login"

db = SQLAlchemy(app)
