from .app import app, db
from flask import render_template, request
from flask_login import login_user, current_user, logout_user, login_required
from .models import get_sample, get_author, Author, User, Book, get_book
from flask_wtf import FlaskForm
from wtforms import StringField, HiddenField, PasswordField
from wtforms.validators import DataRequired
from flask import url_for, redirect
from hashlib import sha256


@app.route("/")
def home():
    return render_template("home.html", books=get_sample(), current_user = current_user)


@app.route("/detail/<id>")
def detail(id):
    books = get_sample()
    book = books[int(id)-1]
    return render_template("detail.html", book=book)

@app.route("/authors/")
def authors():
    return render_template("authors.html", authors=Author.query.all())

@app.route("/books/favoris/")
@login_required
def favoris():
    return render_template("favoris.html", books=Book.query.all())

@app.route("/books/add_to_favoris/<int:id>")
@login_required
def add_book_to_favoris(id):
    book = get_book(id)
    current_user.favoris.append(book)
    db.session.commit()

@app.route("/edit/author/<int:id>")
@login_required
def edit_author(id):
    a = get_author(id)
    f = AuthorForm(id=a.id, name=a.name)
    return render_template("edit-author.html", author=a, form=f)

@app.route("/edit/book/<int:id>")
@login_required
def edit_book(id):
    b = get_book(id)
    f = BookForm(id=b.id, title=b.title, author=b.author)
    return render_template("edit-book.html", book=b, form=f)

@app.route("/save/book/", methods=("POST",))
def save_book():
    b = None
    f = BookForm()
    if f.validate_on_submit():
        id = int(f.id.data)
        b = get_book(id)
        b.title = f.title.data
        db.session.commit()
        return redirect(url_for('edit_book', id=b.id))
    b = get_book(int(f.id.data))
    return render_template("edit-book.html", book=b, form=f)

@app.route("/delete/book/<int:id>")
def delete_book(id):
    b = get_book(id)
    db.session.delete(b)
    db.session.commit()
    return redirect(url_for('home'))

@app.route("/save/author/", methods=("POST",))
def save_author():
    a = None
    f = AuthorForm()
    if f.validate_on_submit():
        id = int(f.id.data)
        a = get_author(id)
        a.name = f.name.data
        db.session.commit()
        return redirect(url_for('edit_author', id=a.id))
    a = get_author(int(f.id.data))
    return render_template("edit-author.html", author=a, form=f)

@app.route("/delete/author/<int:id>")
def delete_author(id):
    a = get_author(id)
    db.session.delete(a)
    db.session.commit()
    return redirect(url_for('home'))

@app.route("/login/", methods=("GET", "POST",))
def login():
    f = LoginForm()
    if not f.is_submitted():
        f.next.data = request.args.get("next")
    elif f.validate_on_submit():
        user = f.get_authenticated_user()
        if user:
            login_user(user)
            next = f.next.data or url_for("home")
            return redirect(next)
    return render_template("login.html", form=f)


@app.route("/logout/")
def logout():
    logout_user()
    return redirect(url_for('home'))


class AuthorForm(FlaskForm):
    id = HiddenField('id')
    name = StringField('Nom', validators=[DataRequired()])

class BookForm(FlaskForm):
    id = HiddenField('id')
    price = StringField('Prix')
    title = StringField('Titre', validators=[DataRequired()])
    url = StringField('URL')
    img = StringField('Image')
    author_id = StringField('Auteur')
    author = StringField('Auteur')

class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    next = HiddenField()

    def get_authenticated_user(self):
        user = User.query.get(self.username.data)
        if user is None:
            return None
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.password else None
